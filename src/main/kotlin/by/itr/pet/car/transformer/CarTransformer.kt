package by.itr.pet.car.transformer

import by.itr.pet.car.dto.CarCreateDto
import by.itr.pet.car.dto.CarDto
import by.itr.pet.car.model.Car
import by.itr.pet.common.transformer.Transformer
import org.springframework.stereotype.Component

@Component
class CarTransformer : Transformer<Car, CarDto> {

    override fun transform(car: Car) = CarDto(
        id = car.id,
        uuid = car.uuid,
        createdAt = car.createdAt,
        updatedAt = car.updatedAt,
        weight = car.weight,
        yearOfManufacture = car.yearOfManufacture,
        engineCapacity = car.engineCapacity,
        color = car.color
    )

    fun transform(dto: CarCreateDto) = Car().apply {
        color = dto.color
        engineCapacity = dto.engineCapacity
        yearOfManufacture = dto.yearOfManufacture
        weight = dto.weight
    }
}