package by.itr.pet.car.controller

import by.itr.pet.car.dto.CarCreateDto
import by.itr.pet.car.service.CarService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/cars"], produces = [(MediaType.APPLICATION_JSON_VALUE)])
class CarController {

    @Autowired
    private lateinit var service: CarService

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody dto: CarCreateDto) =
        service.create(dto)

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun delete(@PathVariable id: Long) =
        service.deleteById(id)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getById(@RequestParam id: Long) =
        service.getById(id)
}