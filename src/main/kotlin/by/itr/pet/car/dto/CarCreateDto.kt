package by.itr.pet.car.dto

import java.time.LocalDateTime

class CarCreateDto {
    lateinit var color: String
    var engineCapacity: Int = 0
    var weight: Int = 0
    lateinit var yearOfManufacture: LocalDateTime
}