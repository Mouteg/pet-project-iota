package by.itr.pet.car.dto

import by.itr.pet.common.dto.Dto
import by.itr.pet.insuranceContract.model.InsuranceContract
import java.time.LocalDateTime

class CarDto(
    var id: Long,
    var uuid: String,
    var createdAt: LocalDateTime,
    var updatedAt: LocalDateTime,
    var color: String,
    var engineCapacity: Int,
    var weight: Int,
    var yearOfManufacture: LocalDateTime,
    var insuranceContract: InsuranceContract? = null
) : Dto
