package by.itr.pet.car.service

import by.itr.pet.car.dto.CarCreateDto
import by.itr.pet.car.dto.CarDto

interface CarService {
    fun create(dto: CarCreateDto): CarDto
    fun deleteById(id: Long)
    fun getById(id: Long): CarDto
}