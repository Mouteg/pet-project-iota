package by.itr.pet.car.service.impl

import by.itr.pet.car.dto.CarCreateDto
import by.itr.pet.car.dto.CarDto
import by.itr.pet.car.repository.CarRepository
import by.itr.pet.car.service.CarService
import by.itr.pet.car.transformer.CarTransformer
import by.itr.pet.common.util.MessageConstants
import javax.persistence.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CarServiceImpl : CarService {

    @Autowired
    private lateinit var repository: CarRepository

    @Autowired
    private lateinit var transformer: CarTransformer

    override fun create(dto: CarCreateDto): CarDto {
        val car = transformer.transform(dto)
        val saved = repository.save(car)
        return transformer.transform(saved)
    }

    override fun deleteById(id: Long) {
        val car = getCarById(id)
        car.hidden = true
        repository.save(car)
    }

    override fun getById(id: Long) =
        transformer.transform(getCarById(id))


    private fun getCarById(id: Long) =
        repository.findById(id).orElseThrow { EntityNotFoundException(MessageConstants.CLIENT_NOT_FOUND) }
}