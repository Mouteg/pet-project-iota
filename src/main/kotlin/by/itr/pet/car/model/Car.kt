package by.itr.pet.car.model

import by.itr.pet.car.model.Car.Companion.CAR_TABLE_NAME
import by.itr.pet.common.model.base.BaseEntity
import by.itr.pet.common.util.now
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import org.apache.logging.log4j.util.Strings.EMPTY

@Entity
@Table(name = CAR_TABLE_NAME)
@SequenceGenerator(name = "sequence", sequenceName = "seq_cars", allocationSize = BaseEntity.ALLOCATION_SIZE)
class Car : BaseEntity() {

    @NotNull
    var color: String = EMPTY

    @NotNull
    @Min(value = 1)
    @Max(value = 100)
    var engineCapacity: Int = 0

    @NotNull
    @Min(value = 60)
    @Max(value = 15000)
    var weight: Int = 0

    @NotNull
    @Past
    var yearOfManufacture: LocalDateTime = now()

    var hidden: Boolean = false

    companion object {
        const val CAR_JOIN_COLUMN = "CAR_ID"
        const val CAR_TABLE_NAME = "CARS"
    }
}