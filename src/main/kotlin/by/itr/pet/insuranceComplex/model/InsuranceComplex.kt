package by.itr.pet.insuranceComplex.model

import by.itr.pet.common.model.base.BaseEntity
import by.itr.pet.insuranceComplex.enum.CoveredPart
import by.itr.pet.insuranceComplex.enum.DamageLevel
import by.itr.pet.insuranceComplex.model.InsuranceComplex.Companion.INSURANCE_COMPLEX_TABLE_NAME
import java.time.Duration
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.validation.constraints.Positive

@Entity
@Table(name = INSURANCE_COMPLEX_TABLE_NAME)
@SequenceGenerator(name = "sequence", sequenceName = "seq_insurance_complexes", allocationSize = BaseEntity.ALLOCATION_SIZE)
class InsuranceComplex : BaseEntity() {

    @Positive
    var duration: Duration = Duration.ZERO

    @Positive
    var compensationPercent: Int = 0

    @Enumerated(EnumType.STRING)
    var damageLevel: DamageLevel = DamageLevel.LOW

    @Enumerated(EnumType.STRING)
    var coveredPart: CoveredPart = CoveredPart.WHEELS

    companion object {
        const val INSURANCE_COMPLEX_TABLE_NAME = "INSURANCE_COMPLEXES"
    }
}