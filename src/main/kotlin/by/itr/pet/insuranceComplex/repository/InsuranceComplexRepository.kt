package by.itr.pet.insuranceComplex.repository

import by.itr.pet.insuranceComplex.model.InsuranceComplex
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InsuranceComplexRepository : JpaRepository<InsuranceComplex, Long>