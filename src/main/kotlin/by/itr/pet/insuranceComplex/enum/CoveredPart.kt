package by.itr.pet.insuranceComplex.enum

enum class CoveredPart {
    WHEELS,
    HULL,
    WINDSCREEN,
    DOORS
}