package by.itr.pet.insuranceComplex.enum

enum class DamageLevel {
    LOW,
    MEDIUM,
    HIGH
}