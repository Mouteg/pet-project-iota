package by.itr.pet.insuranceContract.transformer

import by.itr.pet.insuranceContract.dto.InsuranceContractCreateDto
import by.itr.pet.insuranceContract.model.InsuranceContract
import org.springframework.stereotype.Component

@Component
class InsuranceContractCreateTransformer {

    fun transform(dto: InsuranceContractCreateDto) = InsuranceContract().apply {
        insuranceComplexes = dto.insuranceComplexes
        car = dto.car
        client = dto.client
    }
}