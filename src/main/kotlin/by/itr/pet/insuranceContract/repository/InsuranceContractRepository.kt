package by.itr.pet.insuranceContract.repository

import by.itr.pet.insuranceContract.model.InsuranceContract
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InsuranceContractRepository : JpaRepository<InsuranceContract, Long>