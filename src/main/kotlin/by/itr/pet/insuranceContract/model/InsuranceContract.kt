package by.itr.pet.insuranceContract.model

import by.itr.pet.car.model.Car
import by.itr.pet.car.model.Car.Companion.CAR_JOIN_COLUMN
import by.itr.pet.client.model.Client
import by.itr.pet.insuranceComplex.model.InsuranceComplex
import by.itr.pet.common.model.base.BaseEntity
import by.itr.pet.insuranceContract.model.InsuranceContract.Companion.INSURANCE_CONTRACT_TABLE_NAME
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = INSURANCE_CONTRACT_TABLE_NAME)
@SequenceGenerator(name = "sequence", sequenceName = "seq_insurance_contracts", allocationSize = BaseEntity.ALLOCATION_SIZE)
class InsuranceContract : BaseEntity() {

    @ManyToOne
    lateinit var client: Client

    @OneToOne
    @JoinColumn(name = CAR_JOIN_COLUMN)
    lateinit var car: Car

    @ManyToMany
    lateinit var insuranceComplexes: MutableList<InsuranceComplex>

    val isHidden: Boolean
    get() = car.hidden

    companion object {
        const val INSURANCE_CONTRACT_JOIN_COLUMN = "INSURANCE_CONTRACT_ID"
        const val INSURANCE_CONTRACT_TABLE_NAME = "INSURANCE_CONTRACTS"
    }
}