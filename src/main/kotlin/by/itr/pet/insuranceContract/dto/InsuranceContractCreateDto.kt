package by.itr.pet.insuranceContract.dto

import by.itr.pet.car.model.Car
import by.itr.pet.client.model.Client
import by.itr.pet.insuranceComplex.model.InsuranceComplex

class InsuranceContractCreateDto(
    override var insuranceComplexes: MutableList<InsuranceComplex>,
    override var client: Client,
    override var car: Car
) : InsuranceContractDto(
    client = client,
    car = car,
    insuranceComplexes = insuranceComplexes
)