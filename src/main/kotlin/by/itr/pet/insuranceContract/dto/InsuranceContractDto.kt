package by.itr.pet.insuranceContract.dto

import by.itr.pet.common.dto.Dto
import by.itr.pet.car.model.Car
import by.itr.pet.client.model.Client
import by.itr.pet.insuranceComplex.model.InsuranceComplex
import by.itr.pet.common.util.now
import by.itr.pet.common.util.randomStringUUID
import java.time.LocalDateTime

open class InsuranceContractDto(
    var id: Long = 0,
    var uuid: String = randomStringUUID(),
    var createdAt: LocalDateTime? = now(),
    var updatedAt: LocalDateTime? = now(),
    open var insuranceComplexes: MutableList<InsuranceComplex> = mutableListOf(),
    open var client: Client,
    open var car: Car
) : Dto