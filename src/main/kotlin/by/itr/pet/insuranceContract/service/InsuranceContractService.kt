package by.itr.pet.insuranceContract.service

import by.itr.pet.insuranceContract.dto.InsuranceContractCreateDto

interface InsuranceContractService {
    fun create(dto: InsuranceContractCreateDto)
}