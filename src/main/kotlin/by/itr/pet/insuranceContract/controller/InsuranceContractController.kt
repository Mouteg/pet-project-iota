package by.itr.pet.insuranceContract.controller

import by.itr.pet.insuranceContract.dto.InsuranceContractCreateDto
import by.itr.pet.insuranceContract.service.InsuranceContractService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/insuranceContracts"], produces = [MediaType.APPLICATION_JSON_VALUE])
class InsuranceContractController {

    @Autowired
    private lateinit var service: InsuranceContractService

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody dto: InsuranceContractCreateDto) =
        service.create(dto)
}