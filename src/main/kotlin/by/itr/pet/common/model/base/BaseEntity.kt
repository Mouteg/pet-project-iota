package by.itr.pet.common.model.base

import by.itr.pet.common.util.now
import by.itr.pet.common.util.randomStringUUID
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import javax.persistence.PrePersist
import javax.persistence.PreUpdate

@MappedSuperclass
abstract class BaseEntity : Serializable {
    @Id
    @GeneratedValue(generator = "sequence", strategy = GenerationType.SEQUENCE)
    val id: Long = 0

    val uuid: String = randomStringUUID()

    lateinit var createdAt: LocalDateTime

    lateinit var updatedAt: LocalDateTime

    @PrePersist
    fun prePersist(){
        createdAt = now()
        updatedAt = now()
    }

    @PreUpdate
    fun preUpdate(){
        updatedAt = now()
    }

    companion object {
        const val ALLOCATION_SIZE = 1
    }
}