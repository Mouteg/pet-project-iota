package by.itr.pet.common.util

import java.time.LocalDateTime

fun now(): LocalDateTime {
    return LocalDateTime.now()
}