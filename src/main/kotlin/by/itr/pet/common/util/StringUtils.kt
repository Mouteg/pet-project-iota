package by.itr.pet.common.util

import java.util.UUID

fun randomStringUUID() = UUID.randomUUID().toString()
