package by.itr.pet.common.transformer

import by.itr.pet.common.dto.Dto
import java.io.Serializable

interface Transformer<E : Serializable, D : Dto> {
    fun transform(var1: E): D
}