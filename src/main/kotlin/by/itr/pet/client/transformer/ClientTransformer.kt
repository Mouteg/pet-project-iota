package by.itr.pet.client.transformer

import by.itr.pet.client.dto.ClientCreateDto
import by.itr.pet.client.dto.ClientDto
import by.itr.pet.client.model.Client
import by.itr.pet.common.transformer.Transformer
import org.springframework.stereotype.Component

@Component
class ClientTransformer : Transformer<Client, ClientDto> {
    override fun transform(client: Client) = ClientDto(
        id = client.id,
        uuid = client.uuid,
        createdAt = client.createdAt,
        updatedAt = client.updatedAt,
        email = client.email,
        lastName = client.lastName,
        firstName = client.firstName,
        birthDate = client.birthDate,
        cars = client.cars
    )

    fun transform(dto: ClientCreateDto) = Client().apply {
        email = dto.email
        lastName = dto.lastName
        firstName = dto.firstName
        password = dto.password
        birthDate = dto.birthDate
    }
}