package by.itr.pet.client.service.impl

import by.itr.pet.client.dto.ClientCreateDto
import by.itr.pet.client.dto.ClientDto
import by.itr.pet.client.repository.ClientRepository
import by.itr.pet.client.service.ClientService
import by.itr.pet.client.transformer.ClientTransformer
import by.itr.pet.common.util.MessageConstants
import javax.persistence.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ClientServiceImpl : ClientService {

    @Autowired
    private lateinit var transformer: ClientTransformer

    @Autowired
    private lateinit var repository: ClientRepository

    @Autowired
    private lateinit var passwordEncoder: BCryptPasswordEncoder

    @Transactional
    override fun create(dto: ClientCreateDto): ClientDto {
        dto.password = passwordEncoder.encode(dto.password)
        val client = transformer.transform(dto)
        val save = repository.save(client)
        return transformer.transform(save)
    }

    override fun update(dto: ClientDto): ClientDto {
        val existed = getClientById(dto.id)

        existed.merge(dto)
        repository.save(existed)

        return transformer.transform(existed)
    }

    override fun getById(id: Long) =
        transformer.transform(getClientById(id))

    private fun getClientById(id: Long) =
        repository.findById(id).orElseThrow { EntityNotFoundException(MessageConstants.CLIENT_NOT_FOUND) }
}