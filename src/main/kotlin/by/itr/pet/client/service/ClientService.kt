package by.itr.pet.client.service

import by.itr.pet.client.dto.ClientCreateDto
import by.itr.pet.client.dto.ClientDto

interface ClientService {
    fun create(dto: ClientCreateDto): ClientDto
    fun update(dto: ClientDto): ClientDto
    fun getById(id: Long): ClientDto
}