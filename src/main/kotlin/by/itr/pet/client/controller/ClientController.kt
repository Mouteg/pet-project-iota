package by.itr.pet.client.controller

import by.itr.pet.client.dto.ClientCreateDto
import by.itr.pet.client.dto.ClientDto
import by.itr.pet.client.service.ClientService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/clients"], produces = [(MediaType.APPLICATION_JSON_VALUE)])
class ClientController {

    @Autowired
    private lateinit var service: ClientService

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody dto: ClientCreateDto) =
        service.create(dto)

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    fun update(@RequestBody dto: ClientDto) =
        service.update(dto)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getById(@RequestParam id: Long) =
        service.getById(id)
}