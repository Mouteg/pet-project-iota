package by.itr.pet.client.dto

import by.itr.pet.common.dto.Dto
import by.itr.pet.car.model.Car
import by.itr.pet.insuranceContract.model.InsuranceContract
import by.itr.pet.common.util.randomStringUUID
import java.time.LocalDateTime
import org.apache.logging.log4j.util.Strings

open class ClientDto(
    var id: Long = 0,
    var uuid: String = randomStringUUID(),
    var createdAt: LocalDateTime? = null,
    var updatedAt: LocalDateTime? = null,
    open var email: String = Strings.EMPTY,
    open var lastName: String? = null,
    open var firstName: String? = null,
    open var birthDate: LocalDateTime? = null,
    var cars: MutableList<Car>? = null,
    var insuranceContracts: MutableList<InsuranceContract>? = null
) : Dto