package by.itr.pet.client.dto

import java.time.LocalDateTime
import org.apache.logging.log4j.util.Strings

class ClientCreateDto(
    override var email: String = Strings.EMPTY,
    override var lastName: String? = null,
    override var firstName: String? = null,
    override var birthDate: LocalDateTime? = null,
    var password: String = Strings.EMPTY
) : ClientDto(
    email = email,
    lastName = lastName,
    firstName = firstName,
    birthDate = birthDate
) {

}