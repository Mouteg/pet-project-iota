package by.itr.pet.client.model

import by.itr.pet.car.model.Car
import by.itr.pet.client.dto.ClientDto
import by.itr.pet.car.model.Car.Companion.CAR_JOIN_COLUMN
import by.itr.pet.client.model.Client.Companion.CLIENT_TABLE_NAME
import by.itr.pet.insuranceContract.model.InsuranceContract
import by.itr.pet.insuranceContract.model.InsuranceContract.Companion.INSURANCE_CONTRACT_JOIN_COLUMN
import by.itr.pet.common.model.base.BaseEntity
import by.itr.pet.common.model.base.BaseEntity.Companion.ALLOCATION_SIZE
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.OneToMany
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import org.apache.logging.log4j.util.Strings.EMPTY

@Entity
@Table(name = CLIENT_TABLE_NAME)
@SequenceGenerator(name = "sequence", sequenceName = "seq_clients", allocationSize = ALLOCATION_SIZE)
class Client : BaseEntity() {

    @Email
    var email: String = EMPTY

    var lastName: String? = null

    var firstName: String? = null

    @NotBlank
    var password: String = EMPTY

    @Past
    var birthDate: LocalDateTime? = null

    @OneToMany
    @JoinTable(
        name = CLIENT_CAR_JOIN_TABLE,
        joinColumns = [JoinColumn(name = CLIENT_JOIN_COLUMN)],
        inverseJoinColumns = [JoinColumn(name = CAR_JOIN_COLUMN)]
    )
    var cars: MutableList<Car>? = null

    @OneToMany
    @JoinTable(
        name = CLIENT_INSURANCE_CONTRACT_JOIN_TABLE,
        joinColumns = [JoinColumn(name = CLIENT_JOIN_COLUMN)],
        inverseJoinColumns = [JoinColumn(name = INSURANCE_CONTRACT_JOIN_COLUMN)]
    )
    var insuranceContracts: MutableList<InsuranceContract>? = null

    fun merge(updates: ClientDto) {
        lastName = updates.lastName
        firstName = updates.firstName
        birthDate = updates.birthDate
    }

    companion object {
        const val CLIENT_JOIN_COLUMN = "CLIENT_ID"
        const val CLIENT_TABLE_NAME = "CLIENTS"
        private const val CLIENT_CAR_JOIN_TABLE = "CLIENT_CAR"
        private const val CLIENT_INSURANCE_CONTRACT_JOIN_TABLE = "CLIENT_INSURANCE_CONTRACT"
    }
}